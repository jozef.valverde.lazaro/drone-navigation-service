package application

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jozef.valverde.lazaro/drone-navigation-service/src/domain/services"
	"gitlab.com/jozef.valverde.lazaro/drone-navigation-service/src/domain/value_object"
)

func TestLocationCalculator(t *testing.T) {
	tests := map[string]struct {
		coordinates struct {
			x   string
			y   string
			z   string
			vel string
		}
		expectErr bool
	}{
		"with a valid request body": {
			coordinates: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "456.56",
				z:   "789.89",
				vel: "20.0",
			},
			expectErr: false,
		},
		"with a invalid X axe value": {
			coordinates: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "x",
				y:   "456.56",
				z:   "789.89",
				vel: "20.0",
			},
			expectErr: true,
		},
		"with a invalid Y axe value": {
			coordinates: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "y",
				z:   "789.89",
				vel: "20.0",
			},
			expectErr: true,
		},
		"with a invalid Z axe value": {
			coordinates: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "456.56",
				z:   "z",
				vel: "20.0",
			},
			expectErr: true,
		},
		"with a invalid Vel axe value": {
			coordinates: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "456.56",
				z:   "789.89",
				vel: "vel",
			},
			expectErr: true,
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			calculateLocationService := services.NewCalculateLocationService()
			locCalculator := NewLocationCalculator(calculateLocationService)
			status, response := locCalculator.Invoke(test.coordinates.x, test.coordinates.y, test.coordinates.z, test.coordinates.vel)
			if test.expectErr {
				assert.Equal(t, status, http.StatusBadRequest)
				assert.Equal(t, response, value_object.NewBadRequestError("invalid json body"))
			} else {
				assert.Equal(t, status, http.StatusOK)
				assert.Equal(t, response, value_object.NewResponseLocation(2759.14))
			}
		})
	}
}
