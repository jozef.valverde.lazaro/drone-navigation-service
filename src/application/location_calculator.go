package application

import (
	"net/http"

	"gitlab.com/jozef.valverde.lazaro/drone-navigation-service/src/domain/services"
	"gitlab.com/jozef.valverde.lazaro/drone-navigation-service/src/domain/value_object"
)

// LocationCalculator represents location calculator case
type LocationCalculator struct {
	calculateLocationService *services.CalculateLocationService
}

// NewLocationCalculator represents the instanciation of LocationCalculator
func NewLocationCalculator(calculateLocationService *services.CalculateLocationService) *LocationCalculator {
	return &LocationCalculator{
		calculateLocationService: calculateLocationService,
	}
}

// Invoke is the invocation of the use case
func (uc *LocationCalculator) Invoke(x string, y string, z string, vel string) (status int, response interface{}) {
	loc, err := uc.calculateLocationService.Execute(x, y, z, vel)
	if err != nil {
		return http.StatusBadRequest, value_object.NewBadRequestError("invalid json body")
	}
	return http.StatusOK, value_object.NewResponseLocation(loc)
}
