package value_object

import "strconv"

const (
	sectorID = 2
)

// Coordinates represent coordinates value object
type Coordinates struct {
	x   float64
	y   float64
	z   float64
	vel float64
}

// NewCoordinates is the instantion of Coordinates
func NewCoordinates(x string, y string, z string, vel string) (*Coordinates, error) {
	xAxe, err := strconv.ParseFloat(x, 64)
	if err != nil {
		return nil, err
	}
	yAxe, err := strconv.ParseFloat(y, 64)
	if err != nil {
		return nil, err
	}
	zAxe, err := strconv.ParseFloat(z, 64)
	if err != nil {
		return nil, err
	}
	velocity, err := strconv.ParseFloat(vel, 64)
	if err != nil {
		return nil, err
	}
	return &Coordinates{
		x:   xAxe,
		y:   yAxe,
		z:   zAxe,
		vel: velocity,
	}, nil
}

// GetX return x
func (c Coordinates) GetX() float64 {
	return c.x
}

// GetY return y
func (c Coordinates) GetY() float64 {
	return c.y
}

// GetZ return z
func (c Coordinates) GetZ() float64 {
	return c.z
}

// GetVel return vel
func (c Coordinates) GetVel() float64 {
	return c.vel
}
