package value_object

import "net/http"

// RestErr represent rest errors
type RestErr struct {
	Status  int    `json:"status"`
	Error   string `json:"error"`
	Message string `json:"message"`
}

// NewBadRequestError represent bad request
func NewBadRequestError(msg string) *RestErr {
	return &RestErr{
		Status:  http.StatusBadRequest,
		Error:   "bad_request",
		Message: msg,
	}
}

//NewNotFoundError represent not found error
func NewNotFoundError(msg string) *RestErr {
	return &RestErr{
		Status:  http.StatusNotFound,
		Error:   "not_found",
		Message: msg,
	}
}

//NewInternalServerError represent not found error
func NewInternalServerError(msg string) *RestErr {
	return &RestErr{
		Status:  http.StatusInternalServerError,
		Error:   "internal_error",
		Message: msg,
	}
}

//NewUnprocessableEntity represent when entity cant be process
func NewUnprocessableEntity(msg string) *RestErr {
	return &RestErr{
		Status:  http.StatusUnprocessableEntity,
		Error:   "unprocessable_entity",
		Message: msg,
	}
}
