package value_object

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCoordinates(t *testing.T) {
	tests := map[string]struct {
		coords struct {
			x   string
			y   string
			z   string
			vel string
		}
		expectCoords *Coordinates
		expectErr    bool
	}{
		"with a valid request body": {
			coords: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "456.56",
				z:   "789.89",
				vel: "20.0",
			},
			expectCoords: &Coordinates{
				x:   123.12,
				y:   456.56,
				z:   789.89,
				vel: 20.0,
			},
			expectErr: false,
		},
		"with a invalid X axe value": {
			coords: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "x",
				y:   "456.56",
				z:   "789.89",
				vel: "20.0",
			},
			expectCoords: nil,
			expectErr:    true,
		},
		"with a invalid Y axe value": {
			coords: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "y",
				z:   "789.89",
				vel: "20.0",
			},
			expectCoords: nil,
			expectErr:    true,
		},
		"with a invalid Z axe value": {
			coords: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "456.56",
				z:   "z",
				vel: "20.0",
			},
			expectCoords: nil,
			expectErr:    true,
		},
		"with a invalid Vel axe value": {
			coords: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "456.56",
				z:   "789.89",
				vel: "vel",
			},
			expectCoords: nil,
			expectErr:    true,
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {

			coordinates, err := NewCoordinates(test.coords.x, test.coords.y, test.coords.z, test.coords.vel)
			if test.expectErr {
				assert.Error(t, err)
			} else {
				assert.Nil(t, err)
				assert.Equal(t, coordinates, test.expectCoords)
			}
		})
	}
}
