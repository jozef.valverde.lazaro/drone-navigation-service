package value_object

// ResponseLocation represent rest reponse location
type ResponseLocation struct {
	Loc float64 `json:"loc"`
}

// NewResponseLocation intanciation of response location
func NewResponseLocation(loc float64) *ResponseLocation {
	return &ResponseLocation{
		Loc: loc,
	}
}
