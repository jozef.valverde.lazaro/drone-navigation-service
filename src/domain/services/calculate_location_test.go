package services

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCalculateLocation(t *testing.T) {
	tests := map[string]struct {
		coordinates struct {
			x   string
			y   string
			z   string
			vel string
		}
		expectErr bool
	}{
		"with a valid request body": {
			coordinates: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "456.56",
				z:   "789.89",
				vel: "20.0",
			},
			expectErr: false,
		},
		"with a invalid X axe value": {
			coordinates: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "x",
				y:   "456.56",
				z:   "789.89",
				vel: "20.0",
			},
			expectErr: true,
		},
		"with a invalid Y axe value": {
			coordinates: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "y",
				z:   "789.89",
				vel: "20.0",
			},
			expectErr: true,
		},
		"with a invalid Z axe value": {
			coordinates: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "456.56",
				z:   "z",
				vel: "20.0",
			},
			expectErr: true,
		},
		"with a invalid Vel axe value": {
			coordinates: struct {
				x   string
				y   string
				z   string
				vel string
			}{
				x:   "123.12",
				y:   "456.56",
				z:   "789.89",
				vel: "vel",
			},
			expectErr: true,
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {

			calculateLocationService := NewCalculateLocationService()
			loc, err := calculateLocationService.Execute(test.coordinates.x, test.coordinates.y, test.coordinates.z, test.coordinates.vel)
			if test.expectErr {
				assert.Error(t, err)
			} else {
				assert.Nil(t, err)
				assert.Equal(t, loc, 2759.14)
			}
		})
	}
}
