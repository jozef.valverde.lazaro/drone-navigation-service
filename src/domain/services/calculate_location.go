package services

import (
	"math"

	"gitlab.com/jozef.valverde.lazaro/drone-navigation-service/src/domain/value_object"
)

const (
	sectorID = 2
)

// CalculateLocationService represent the calculate location service
type CalculateLocationService struct {
}

// NewCalculateLocationService is the instanciation of this service
func NewCalculateLocationService() *CalculateLocationService {
	return &CalculateLocationService{}
}

// Execute is the exec of this service
func (s CalculateLocationService) Execute(x string, y string, z string, vel string) (float64, error) {
	coordinates, err := value_object.NewCoordinates(x, y, z, vel)
	if err != nil {
		return 0, err
	}
	loc := (coordinates.GetX()+coordinates.GetY()+coordinates.GetZ())*sectorID + coordinates.GetVel()
	roundedLoc := math.Round(loc*100) / 100
	return roundedLoc, nil
}
