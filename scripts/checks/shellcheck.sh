#!/bin/bash

# Check shell scripts
# Intended to be run from local machine or CI

set -eufo pipefail
IFS=$'\t\n'

# Check required commands are in place
command -v shellcheck > /dev/null 2>&1 || { echo 'please install shellcheck or use image that has it'; exit 1; }

echo '> Checking shell scripts for potential errors'
find . -type f -name "*.sh" ! -path "./.cache/*" -print0 | \
	  xargs -0 shellcheck
