#!/bin/bash

# Check tests
# Intended to be run from local machine or CI

set -eufo pipefail
IFS=$'\t\n'

# If tests are being running outside CI prefer to use richgo to enrich outputs with text decorations
go='go'
if [[ -z "${GITLAB_CI:-}" ]]; then
    go='richgo'
fi

# Check required commands are in place
command -v $go > /dev/null 2>&1 || { echo "please install $go or use image that has it"; exit 1; }

echo '> Running unit tests'
$go test ./... -race -coverprofile=.test_coverage.out
go tool cover -func=.test_coverage.out | tail -n1 | awk '{print "Total test coverage: " $3}'
