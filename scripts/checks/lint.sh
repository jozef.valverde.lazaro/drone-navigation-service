#!/bin/bash

# Check style
# Intended to be run from local machine or CI

set -eufo pipefail
IFS=$'\t\n'

# Check required commands are in place
command -v golint > /dev/null 2>&1 || { echo 'please install golint or use image that has it'; exit 1; }

echo '> Checking for style errors'
golint -set_exit_status ./...
