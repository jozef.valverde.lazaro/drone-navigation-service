#!/bin/bash

# Check static analysis
# Intended to be run from local machine or CI

set -eufo pipefail
IFS=$'\t\n'

# Check required commands are in place
command -v staticcheck > /dev/null 2>&1 || { echo 'please install staticcheck or use image that has it'; exit 1; }

echo '> Running static checks'
staticcheck ./...
