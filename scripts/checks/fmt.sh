#!/bin/bash

# Check imports
# Intended to be run from local machine or CI

set -eufo pipefail
IFS=$'\t\n'

# Check required commands are in place
command -v goimports > /dev/null 2>&1 || { echo 'please install goimports or use image that has it'; exit 1; }

echo '> Checking Go file formatting'

# shellcheck disable=SC2046
out="$(goimports -l $(find . -name '*.go' -not -path './.cache/*' -not -path './internal/mock/*'))"
[[ -z "$out" ]] || { echo "$out"; exit 42; }
