#!/bin/bash

# Check for unchecked errors
# Intended to be run from local machine or CI

set -eufo pipefail
IFS=$'\t\n'

# Check required commands are in place
command -v errcheck > /dev/null 2>&1 || { echo 'please install errcheck or use image that has it'; exit 1; }

echo '> Checking for ignored Go errors'
errcheck -ignoretests ./...
