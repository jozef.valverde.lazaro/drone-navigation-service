.PHONY: help check test coverage fmt fix-fmt vet lint staticcheck errcheck tidy shellcheck compile install-tools install-deps

help: ## Show this help
	@echo
	@echo "Help"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "    \033[36m%-20s\033[93m %s\n", $$1, $$2}'
	@echo

##
### Code validation
check: ## Run all checks: fmt vet lint errcheck staticcheck tidy test
check: fmt vet lint errcheck staticcheck tidy shellcheck test

test: ## Run tests with coverage
	@bash scripts/checks/test.sh

coverage: ## Run tests and open coverage report
coverage: test
	@go tool cover -html=.test_coverage.out

fmt: ## Run goimports on all packages, printing files that don't match code-format if any
	@bash scripts/checks/fmt.sh

fix-fmt: ## Run goimports on all packages, fix files that don't match code-style
	@bash scripts/local/fix-fmt.sh

vet: ## Run vet on all packages (more info running `go doc cmd/vet`)
	@bash scripts/checks/vet.sh

lint: ## Run lint on the codebase, printing any style errors
	@bash scripts/checks/lint.sh

staticcheck: ## Run staticcheck on the codebase
	@bash scripts/checks/staticcheck.sh

errcheck: ## Run errcheck to find ignored errors
	@bash scripts/checks/errcheck.sh

tidy: ## Check for no-longer-needed dependencies
	@bash scripts/checks/tidy.sh

fix-tidy: ## Fix go.mod inconsistency
	@bash scripts/local/fix-tidy.sh

shellcheck: ## Lint shell scripts for potential errors
	@bash scripts/checks/shellcheck.sh
##
### Compile and install dependencies
compile: ## Compile the binary
	@bash scripts/compile.sh

install-tools: ## Install external tools
	@bash scripts/install-tools.sh

install-deps: ## Prefetch deps to ensure required versions are downloaded
	@go mod tidy
	@go mod verify