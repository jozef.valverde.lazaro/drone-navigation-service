package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/jozef.valverde.lazaro/drone-navigation-service/src/application"
	"gitlab.com/jozef.valverde.lazaro/drone-navigation-service/src/domain/services"
	"gitlab.com/jozef.valverde.lazaro/drone-navigation-service/src/domain/value_object"
)

// RequestBody represents the request body
type RequestBody struct {
	X   string `json:"x"`
	Y   string `json:"y"`
	Z   string `json:"z"`
	Vel string `json:"vel"`
}

//CalculateLocationController todo
func CalculateLocationController(c *gin.Context) {
	var req RequestBody
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, value_object.NewBadRequestError("invalid json body"))
		return
	}
	calculateLocationService := services.NewCalculateLocationService()
	locCalculator := application.NewLocationCalculator(calculateLocationService)
	status, response := locCalculator.Invoke(req.X, req.Y, req.Z, req.Vel)
	c.JSON(status, response)
}
