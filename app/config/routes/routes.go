package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/jozef.valverde.lazaro/drone-navigation-service/app/src/controller"
)

//Router represents router
type Router struct {
	engine *gin.Engine
}

// NewRouter instantiate new router
func NewRouter(engine *gin.Engine) *Router {
	return &Router{
		engine: engine,
	}
}

// URLMapping map endpoints
func (r *Router) URLMapping() {
	r.engine.POST("/api/dns/calculateLocation", controller.CalculateLocationController)
	r.engine.Run()
}
