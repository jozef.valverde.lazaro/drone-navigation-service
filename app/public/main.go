package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/jozef.valverde.lazaro/drone-navigation-service/app/config/routes"
)

func main() {
	engine := gin.Default()
	router := routes.NewRouter(engine)
	router.URLMapping()
}
