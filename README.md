# drone-navigation-service

Drone Navigation API is build following **Domaing Driver Design** which is supported by **Hexagonal Pattern** and **SOLID principles**

- [Getting Started](#getting-started)
  * [Requirements](#requirements)
  * [Installation](#installation)
    + [Check Tools](#check-tools)
    + [Project Dependencies](#project-dependencies)
- [Testing](#testing)
- [Compile and Run](#compile-and-run)
- [Sending a Reuqest](#sending-a-request)

## Getting Started

### Requirements

- golang 1.12
- [shellcheck](https://github.com/koalaman/shellcheck#installing)

### Installation

#### Check Tools

In order to make all the checks in this codebase, there are some external go tools that must be installed:

```sh
$ make install-tools
```

**PS:** remember to update your PATH correctly: `export PATH=$(go env GOPATH)/bin:$PATH`

#### Project Dependencies

```sh
$ make install-deps
```

## Testing

```sh
$ make test
```

If you prefer to run unit tests and visualize a detailed coverage report, instead the above command you can do:

```sh
$ make coverage
```

## Compile and Run

## Local Environment with docker
```sh
$ docker build -t dns .
$ docker run -p 8080:8080 dns
```

### Local Environment development

```sh
$ go run app/public/main.go
```

## Sending a Request
```sh
$ curl -X POST --data '{"x": "123.12", "y": "456.56", "z": "789.89", "vel": "20.0"}' -H "Content-Type: application/json" http://localhost:8080/api/dns/calculateLocation
```