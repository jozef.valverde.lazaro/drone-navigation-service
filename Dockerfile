FROM golang:1.12 
RUN mkdir /dns 
ADD . /dns/ 
WORKDIR /dns
RUN export PATH="$GOPATH/bin:$PATH"
RUN go build app/public/main.go 
CMD ["./main"]